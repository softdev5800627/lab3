/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattarapol.lab3;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
class OXProgram {
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;
    private static String isContinue;
     public static void main(String[] args) {
        printWelcome();
        while(true){
        printTable();
        printTurn();
        inputRowCol();
            if(checkWin(table,currentPlayer)){
                printTable();
                printWin();
                break;
             
            } else if(checkDraw(table,currentPlayer)){
                printTable();
                printDraw();
                break;
            }
         switchPlayer(); 
        }
        inputContinue();
        
    }

    private static void printWelcome() {
        System.out.println("Welcome To OX Game");
        System.out.println("—-----------------");
    }

    private static void printTable() {
        for(int i = 0 ; i <3 ; i++){
            System.out.print("|");
            for(int j =0 ; j <3 ; j++){
                System.out.print(table[i][j]+"|");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.print(currentPlayer + " turn.");
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.print("Please input row[1-3] and column[1-3]: ");
        row = kb.nextInt()-1;
        col = kb.nextInt()-1;
         if(row >=0 && row <3 && col >= 0 &&col < 3 &&table[row][col]== '-'  ){  
             table[row][col] = currentPlayer;  
             return;
        }
        }
        
    }

    private static void switchPlayer() {
        if(currentPlayer == 'X'){
            currentPlayer = 'O';
        }else{
            currentPlayer = 'X';
        }
    }

    static boolean checkWin(char[][] table, char currentPlayer) {
        if (checkRow(table, currentPlayer)||checkCol(table,currentPlayer)||checkDiagonal1(table,currentPlayer)||checkDiagonal2(table,currentPlayer)) {
            return true;
        }
           return false;
           
      
    }
     static boolean checkDraw(char[][] table, char currentPlayer ) {
        for(int i = 0 ; i < 3; i++ ){
            for(int j =0 ; j <3 ; j++){
                if(table[i][j] == '-'){
                    return  false;
                    
                }
            }
        }
        return true;
    }

    private static boolean checkRow(char[][] table, char currentPlayer) {
        for (int row = 0; row < 3; row ++) {
            if (checkRow(table,currentPlayer,row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(char[][] table, char currentPlayer, int row) {
        return table[row][0] == currentPlayer  && table[row][1]== currentPlayer && table[row][2]== currentPlayer ; 
    }
    private static boolean checkCol(char[][] table, char currentPlayer) {
        for (int col = 0; col < 3; col ++) {
            if (checkCol(table,currentPlayer,col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(char[][] table, char currentPlayer, int col) {
      return table[0][col] == currentPlayer && table[1][col] == currentPlayer && table[2][col] == currentPlayer ;
    } 
   

    private static boolean checkDiagonal1(char[][] table, char currentPlayer) {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;


    }

    private static boolean checkDiagonal2(char[][] table, char currentPlayer) {
    if(table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer){
            return true;     
        }
        return false;     
    }
    
   
     private static void printWin() {
        System.out.println("Congratulations! Player " + currentPlayer +" wins!");
    }
     private static void printDraw() {
          System.out.println("The game is a draw!");
    }

    private static void inputContinue() {
        Scanner kb = new Scanner(System.in);
       while (true) {
           System.out.print("Do you want to play again? (y/n) :");
           isContinue = kb.next();
           if(isContinue.equalsIgnoreCase("y")){
               resetGame();
               main(null);
               break;
              } else if (isContinue.equalsIgnoreCase("n")) {
                System.out.println("Thank you for playing! Goodbye!");
                break;
            } else {
                System.out.println("Invalid input. Please enter 'y' or 'n'.");
            }
        }
    }

     private static void resetGame() {
        table = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        currentPlayer = 'X';
    }


    }
    




    
    

